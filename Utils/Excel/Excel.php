<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace phpOMS\Utils\Excel;

/** @noinspection PhpIncludeInspection */
require_once realpath(__DIR__ . '/../../../vendor/PHPExcel/Classes/PHPExcel.php');

/**
 * Excel class.
 *
 * @category   Modules
 * @package    phpOMS\Utils\Excel
 * @since      1.0.0
 */
/** @noinspection PhpUndefinedClassInspection */
class Excel extends \PHPExcel
{
}
