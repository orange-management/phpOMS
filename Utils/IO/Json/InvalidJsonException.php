<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace phpOMS\Utils\IO\Json;

/**
 * Json decoding exception class.
 *
 * @category   Framework
 * @package    IO
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class InvalidJsonException extends \UnexpectedValueException
{
    public function __construct($message, $code = 0, \Exception $previous = null)
    {
        parent::__construct('Couldn\'t parse "' . $message . '" as valid json.', $code, $previous);
    }
}
