<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace phpOMS\Message;

use phpOMS\Datatypes\Enum;

/**
 * Request method enum.
 *
 * @category   Request
 * @package    Framework
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
abstract class RequestMethod extends Enum
{
    const GET    = 'GET';    /* GET */
    const POST   = 'POST';   /* POST */
    const PUT    = 'PUT';    /* PUT */
    const DELETE = 'DELETE'; /* DELETE */
    const HEAD   = 'HEAD';   /* HEAD */
    const TRACE  = 'TRACE';  /* TRACE */
}
