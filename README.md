# General

The Orange Management software is a modular web application for small to mid sized companies that need CRM, ERP, Intranet and/or CMS features. The Orange Management modules can be split into the following different categories:

* General
* Business
* Education
* Health Care

There is no limitation on how to combine modules. The modules are structured in a way that there is a minimum amount of dependencies between modules. Often modules provide additional functionality for other modules or modules request features from existing modules. Only in a few cases modules require another module for it's functionality, this is usually only the case for core modules or modules that have a strong relation to an other module.

Thanks to the modular structure it's easy to have full control over the costs and keep the functionality to the exact amount that is needed. There is no fear of having too many functions that reduce the usability if they are not needed. This also results in a fast environment not just because only the necessary parts get installed but also because the core is built for a good experience in terms of performance.

## Pricing & Services

The current pricing model is single payment for the core application and modules. All updates regarding security and software fixes are free for the core application and all modules. Only enhancements, visually and in terms of functionality require a one time payment for that update. This way it's also possible to have full control over software changes and their costs. For convenience reasons Orange Management also offers various module bundles and upgrade subscriptions for updates during that subscription.

Additional optional services Orange Management provides are:

* Initial software setup with all purchased modules
* Regular maintenance
* Customization of all modules provided by Orange Management
* Hosting

## Development Status

Currently Orange Management is still fully developing the first Alpha version. As soon as we have a running Beta version we are allowing external testers to use our software and a selected amount of inhouse developed modules.

## Jobs

We are always looking for people that are interested in joining this project. Unfortunately our current financial situation doesn't leave any room for payed staff members. All we can offer right now is a share of our future income and a great time. We are looking for:

* PHP developer
* JavaScript developer
* Frontend developer
* Designer

Are you interested in joining us? Feel free to contact us at spl1nes.com@gmail.com.

## Overview

* Project: Orange Management
* Group: Orange Management
* Developers: 1
* Languages: PHP, JS, Java, HTML, CSS
* Dependencies: d3.js, THREE.js, tcpdf, PhpExcel

### Build info

Build infos are getting generated automatically for every build and manually for key commits.

#### Metrics
[LOC Framework](http://orange-management.de/Build/stats/phpOMS.log) - 
[LOC Modules](http://orange-management.de/Build/stats/ModulesStats.log) - 
[Metrics Framework](http://orange-management.de/Build/stats/ReportFramework.html) - 
[Metrics Modules](http://orange-management.de/Build/stats/ReportModules.html) - 

#### Code quality
[PhpUnit Framework](http://orange-management.de/Build/logs/phpunit.log) - 
[PhpCPD Framework](http://orange-management.de/Build/logs/phpcpdFramework.log) - 
[PhpCPD Modules](http://orange-management.de/Build/logs/phpcpdModules.log) - 

#### Linting
[Php Core](http://orange-management.de/Build/logs/phpLintFramework.log) - 
[Php Modules](http://orange-management.de/Build/logs/phpLintModules.log) - 
[Json](http://orange-management.de/Build/logs/jsonLint.log)

#### Code style
[PhpCS Framework](http://orange-management.de/Build/logs/phpcsFramework.log) - 
[PhpCS Modules](http://orange-management.de/Build/logs/phpcsModules.log) - 
[Html Tags](http://orange-management.de/Build/logs/htmlinspection.log) - 
[Empty Attributes](http://orange-management.de/Build/logs/unusedattributes.log)
