<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace phpOMS\Localization;

use phpOMS\Datatypes\Enum;

/**
 * Country codes ISO list.
 *
 * @category   Framework
 * @package    phpOMS\DataStorage\Database
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class ISO4217CharEnum extends Enum
{
    const C_ALL = 'ALL'; 

    const C_AFN = 'AFN'; 

    const C_ARS = 'ARS'; 

    const C_AWG = 'AWG'; 

    const C_AUD = 'AUD'; 

    const C_AZN = 'AZN'; 

    const C_BSD = 'BSD'; 

    const C_BBD = 'BBD'; 

    const C_BYR = 'BYR'; 

    const C_BZD = 'BZD'; 

    const C_BMD = 'BMD'; 

    const C_BOB = 'BOB'; 

    const C_BAM = 'BAM'; 

    const C_BWP = 'BWP'; 

    const C_BGN = 'BGN'; 

    const C_BRL = 'BRL'; 

    const C_BND = 'BND'; 

    const C_KHR = 'KHR'; 

    const C_CAD = 'CAD'; 

    const C_KYD = 'KYD'; 

    const C_CLP = 'CLP'; 

    const C_CNY = 'CNY'; 

    const C_COP = 'COP'; 

    const C_CRC = 'CRC'; 

    const C_HRK = 'HRK'; 

    const C_CUP = 'CUP'; 

    const C_CZK = 'CZK'; 

    const C_DKK = 'DKK'; 

    const C_DOP = 'DOP'; 

    const C_XCD = 'XCD'; 

    const C_EGP = 'EGP'; 

    const C_SVC = 'SVC'; 

    const C_EEK = 'EEK'; 

    const C_EUR = 'EUR';     

    const C_FKP = 'FKP'; 

    const C_FJD = 'FJD'; 

    const C_GHC = 'GHC'; 

    const C_GIP = 'GIP'; 

    const C_GTQ = 'GTQ'; 

    const C_GGP = 'GGP'; 

    const C_GYD = 'GYD'; 

    const C_HNL = 'HNL'; 

    const C_HKD = 'HKD'; 

    const C_HUF = 'HUF'; 

    const C_ISK = 'ISK'; 

    const C_INR = 'INR'; 

    const C_IDR = 'IDR'; 

    const C_IRR = 'IRR'; 

    const C_IMP = 'IMP'; 

    const C_ILS = 'ILS'; 

    const C_JMD = 'JMD'; 

    const C_JPY = 'JPY'; 

    const C_JEP = 'JEP'; 

    const C_KZT = 'KZT'; 

    const C_KES = 'KES'; 

    const C_KGS = 'KGS'; 

    const C_LAK = 'LAK'; 

    const C_LVL = 'LVL'; 

    const C_LBP = 'LBP'; 

    const C_LRD = 'LRD'; 

    const C_LTL = 'LTL'; 

    const C_MKD = 'MKD'; 

    const C_MYR = 'MYR'; 

    const C_MUR = 'MUR'; 

    const C_MXN = 'MXN'; 

    const C_MNT = 'MNT'; 

    const C_MZN = 'MZN'; 

    const C_NAD = 'NAD'; 

    const C_NPR = 'NPR'; 

    const C_ANG = 'ANG'; 

    const C_NZD = 'NZD'; 

    const C_NIO = 'NIO'; 

    const C_NGN = 'NGN'; 

    const C_KPW = 'KPW'; 

    const C_NOK = 'NOK'; 

    const C_OMR = 'OMR'; 

    const C_PKR = 'PKR'; 

    const C_PAB = 'PAB'; 

    const C_PYG = 'PYG'; 

    const C_PEN = 'PEN'; 

    const C_PHP = 'PHP'; 

    const C_PLN = 'PLN'; 

    const C_QAR = 'QAR'; 

    const C_RON = 'RON'; 

    const C_RUB = 'RUB'; 

    const C_SHP = 'SHP'; 

    const C_SAR = 'SAR'; 

    const C_RSD = 'RSD'; 

    const C_SCR = 'SCR'; 

    const C_SGD = 'SGD'; 

    const C_SBD = 'SBD'; 

    const C_SOS = 'SOS'; 

    const C_ZAR = 'ZAR'; 

    const C_KRW = 'KRW'; 

    const C_LKR = 'LKR'; 

    const C_SEK = 'SEK'; 

    const C_CHF = 'CHF'; 

    const C_SRD = 'SRD'; 

    const C_SYP = 'SYP'; 

    const C_TWD = 'TWD'; 

    const C_THB = 'THB'; 

    const C_TTD = 'TTD'; 

    const C_TRY = 'TRY'; 

    const C_TRL = 'TRL'; 

    const C_TVD = 'TVD'; 

    const C_UAH = 'UAH'; 

    const C_GBP = 'GBP'; 

    const C_USD = 'USD'; 

    const C_UYU = 'UYU'; 

    const C_UZS = 'UZS'; 

    const C_VEF = 'VEF'; 

    const C_VND = 'VND'; 

    const C_YER = 'YER'; 

    const C_ZWD = 'ZWD'; 
}
