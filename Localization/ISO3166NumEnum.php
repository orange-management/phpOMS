<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace phpOMS\Localization;

use phpOMS\Datatypes\Enum;

/**
 * Country codes ISO list.
 *
 * @category   Framework
 * @package    phpOMS\DataStorage\Database
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class ISO3166NumEnum extends Enum
{
    const C_AFG = '004';
    const C_ALA = '248';
    const C_ALB = '008';
    const C_DZA = '012';
    const C_ASM = '016';
    const C_AND = '020';
    const C_AGO = '024';
    const C_AIA = '660';
    const C_ATA = '010';
    const C_ATG = '028';
    const C_ARG = '032';
    const C_ARM = '051';
    const C_ABW = '533';
    const C_AUS = '036';
    const C_AUT = '040';
    const C_AZE = '031';
    const C_BHS = '044';
    const C_BHR = '048';
    const C_BGD = '050';
    const C_BRB = '052';
    const C_BLR = '112';
    const C_BEL = '056';
    const C_BLZ = '084';
    const C_BEN = '204';
    const C_BMU = '060';
    const C_BTN = '064';
    const C_BOL = '068';
    const C_BES = '535';
    const C_BIH = '070';
    const C_BWA = '072';
    const C_BVT = '074';
    const C_BRA = '076';
    const C_IOT = '086';
    const C_BRN = '096';
    const C_BGR = '100';
    const C_BFA = '854';
    const C_BDI = '108';
    const C_CPV = '132';
    const C_KHM = '116';
    const C_CMR = '120';
    const C_CAN = '124';
    const C_CYM = '136';
    const C_CAF = '140';
    const C_TCD = '148';
    const C_CHL = '152';
    const C_CHN = '156';
    const C_CXR = '162';
    const C_CCK = '166';
    const C_COL = '170';
    const C_COM = '174';
    const C_COG = '178';
    const C_COD = '180';
    const C_COK = '184';
    const C_CRI = '188';
    const C_CIV = '384';
    const C_HRV = '191';
    const C_CUB = '192';
    const C_CUW = '531';
    const C_CYP = '196';
    const C_CZE = '203';
    const C_DNK = '208';
    const C_DJI = '262';
    const C_DMA = '212';
    const C_DOM = '214';
    const C_ECU = '218';
    const C_EGY = '818';
    const C_SLV = '222';
    const C_GNQ = '226';
    const C_ERI = '232';
    const C_EST = '233';
    const C_ETH = '231';
    const C_FLK = '238';
    const C_FRO = '234';
    const C_FJI = '242';
    const C_FIN = '246';
    const C_FRA = '250';
    const C_GUF = '254';
    const C_PYF = '258';
    const C_ATF = '260';
    const C_GAB = '266';
    const C_GMB = '270';
    const C_GEO = '268';
    const C_DEU = '276';
    const C_GHA = '288';
    const C_GIB = '292';
    const C_GRC = '300';
    const C_GRL = '304';
    const C_GRD = '308';
    const C_GLP = '312';
    const C_GUM = '316';
    const C_GTM = '320';
    const C_GGY = '831';
    const C_GIN = '324';
    const C_GNB = '624';
    const C_GUY = '328';
    const C_HTI = '332';
    const C_HMD = '334';
    const C_VAT = '336';
    const C_HND = '340';
    const C_HKG = '344';
    const C_HUN = '348';
    const C_ISL = '352';
    const C_IND = '356';
    const C_IDN = '360';
    const C_IRN = '364';
    const C_IRQ = '368';
    const C_IRL = '372';
    const C_IMN = '833';
    const C_ISR = '376';
    const C_ITA = '380';
    const C_JAM = '388';
    const C_JPN = '392';
    const C_JEY = '832';
    const C_JOR = '400';
    const C_KAZ = '398';
    const C_KEN = '404';
    const C_KIR = '296';
    const C_PRK = '408';
    const C_KOR = '410';
    const C_KWT = '414';
    const C_KGZ = '417';
    const C_LAO = '418';
    const C_LVA = '428';
    const C_LBN = '422';
    const C_LSO = '426';
    const C_LBR = '430';
    const C_LBY = '434';
    const C_LIE = '438';
    const C_LTU = '440';
    const C_LUX = '442';
    const C_MAC = '446';
    const C_MKD = '807';
    const C_MDG = '450';
    const C_MWI = '454';
    const C_MYS = '458';
    const C_MDV = '462';
    const C_MLI = '466';
    const C_MLT = '470';
    const C_MHL = '584';
    const C_MTQ = '474';
    const C_MRT = '478';
    const C_MUS = '480';
    const C_MYT = '175';
    const C_MEX = '484';
    const C_FSM = '583';
    const C_MDA = '498';
    const C_MCO = '492';
    const C_MNG = '496';
    const C_MNE = '499';
    const C_MSR = '500';
    const C_MAR = '504';
    const C_MOZ = '508';
    const C_MMR = '104';
    const C_NAM = '516';
    const C_NRU = '520';
    const C_NPL = '524';
    const C_NLD = '528';
    const C_NCL = '540';
    const C_NZL = '554';
    const C_NIC = '558';
    const C_NER = '562';
    const C_NGA = '566';
    const C_NIU = '570';
    const C_NFK = '574';
    const C_MNP = '580';
    const C_NOR = '578';
    const C_OMN = '512';
    const C_PAK = '586';
    const C_PLW = '585';
    const C_PSE = '275';
    const C_PAN = '591';
    const C_PNG = '598';
    const C_PRY = '600';
    const C_PER = '604';
    const C_PHL = '608';
    const C_PCN = '612';
    const C_POL = '616';
    const C_PRT = '620';
    const C_PRI = '630';
    const C_QAT = '634';
    const C_REU = '638';
    const C_ROU = '642';
    const C_RUS = '643';
    const C_RWA = '646';
    const C_BLM = '652';
    const C_SHN = '654';
    const C_KNA = '659';
    const C_LCA = '662';
    const C_MAF = '663';
    const C_SPM = '666';
    const C_VCT = '670';
    const C_WSM = '882';
    const C_SMR = '674';
    const C_STP = '678';
    const C_SAU = '682';
    const C_SEN = '686';
    const C_SRB = '688';
    const C_SYC = '690';
    const C_SLE = '694';
    const C_SGP = '702';
    const C_SXM = '534';
    const C_SVK = '703';
    const C_SVN = '705';
    const C_SLB = '090';
    const C_SOM = '706';
    const C_ZAF = '710';
    const C_SGS = '239';
    const C_SSD = '728';
    const C_ESP = '724';
    const C_LKA = '144';
    const C_SDN = '729';
    const C_SUR = '740';
    const C_SJM = '744';
    const C_SWZ = '748';
    const C_SWE = '752';
    const C_CHE = '756';
    const C_SYR = '760';
    const C_TWN = '158';
    const C_TJK = '762';
    const C_TZA = '834';
    const C_THA = '764';
    const C_TLS = '626';
    const C_TGO = '768';
    const C_TKL = '772';
    const C_TON = '776';
    const C_TTO = '780';
    const C_TUN = '788';
    const C_TUR = '792';
    const C_TKM = '795';
    const C_TCA = '796';
    const C_TUV = '798';
    const C_UGA = '800';
    const C_UKR = '804';
    const C_ARE = '784';
    const C_GBR = '826';
    const C_USA = '840';
    const C_UMI = '581';
    const C_URY = '858';
    const C_UZB = '860';
    const C_VUT = '548';
    const C_VEN = '862';
    const C_VNM = '704';
    const C_VGB = '092';
    const C_VIR = '850';
    const C_WLF = '876';
    const C_ESH = '732';
    const C_YEM = '887';
    const C_ZMB = '894';
    const C_ZWE = '716';
}