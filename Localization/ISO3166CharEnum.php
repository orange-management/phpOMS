<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace phpOMS\Localization;

use phpOMS\Datatypes\EnumArray;

/**
 * Country codes ISO list.
 *
 * @category   Framework
 * @package    phpOMS\DataStorage\Database
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class ISO3166CharEnum extends EnumArray
{
    const C_AFG = 'AFG';
    const C_ALA = 'ALA';
    const C_ALB = 'ALB';
    const C_DZA = 'DZA';
    const C_ASM = 'ASM';
    const C_AND = 'AND';
    const C_AGO = 'AGO';
    const C_AIA = 'AIA';
    const C_ATA = 'ATA';
    const C_ATG = 'ATG';
    const C_ARG = 'ARG';
    const C_ARM = 'ARM';
    const C_ABW = 'ABW';
    const C_AUS = 'AUS';
    const C_AUT = 'AUT';
    const C_AZE = 'AZE';
    const C_BHS = 'BHS';
    const C_BHR = 'BHR';
    const C_BGD = 'BGD';
    const C_BRB = 'BRB';
    const C_BLR = 'BLR';
    const C_BEL = 'BEL';
    const C_BLZ = 'BLZ';
    const C_BEN = 'BEN';
    const C_BMU = 'BMU';
    const C_BTN = 'BTN';
    const C_BOL = 'BOL';
    const C_BES = 'BES';
    const C_BIH = 'BIH';
    const C_BWA = 'BWA';
    const C_BVT = 'BVT';
    const C_BRA = 'BRA';
    const C_IOT = 'IOT';
    const C_BRN = 'BRN';
    const C_BGR = 'BGR';
    const C_BFA = 'BFA';
    const C_BDI = 'BDI';
    const C_CPV = 'CPV';
    const C_KHM = 'KHM';
    const C_CMR = 'CMR';
    const C_CAN = 'CAN';
    const C_CYM = 'CYM';
    const C_CAF = 'CAF';
    const C_TCD = 'TCD';
    const C_CHL = 'CHL';
    const C_CHN = 'CHN';
    const C_CXR = 'CXR';
    const C_CCK = 'CCK';
    const C_COL = 'COL';
    const C_COM = 'COM';
    const C_COG = 'COG';
    const C_COD = 'COD';
    const C_COK = 'COK';
    const C_CRI = 'CRI';
    const C_CIV = 'CIV';
    const C_HRV = 'HRV';
    const C_CUB = 'CUB';
    const C_CUW = 'CUW';
    const C_CYP = 'CYP';
    const C_CZE = 'CZE';
    const C_DNK = 'DNK';
    const C_DJI = 'DJI';
    const C_DMA = 'DMA';
    const C_DOM = 'DOM';
    const C_ECU = 'ECU';
    const C_EGY = 'EGY';
    const C_SLV = 'SLV';
    const C_GNQ = 'GNQ';
    const C_ERI = 'ERI';
    const C_EST = 'EST';
    const C_ETH = 'ETH';
    const C_FLK = 'FLK';
    const C_FRO = 'FRO';
    const C_FJI = 'FJI';
    const C_FIN = 'FIN';
    const C_FRA = 'FRA';
    const C_GUF = 'GUF';
    const C_PYF = 'PYF';
    const C_ATF = 'ATF';
    const C_GAB = 'GAB';
    const C_GMB = 'GMB';
    const C_GEO = 'GEO';
    const C_DEU = 'DEU';
    const C_GHA = 'GHA';
    const C_GIB = 'GIB';
    const C_GRC = 'GRC';
    const C_GRL = 'GRL';
    const C_GRD = 'GRD';
    const C_GLP = 'GLP';
    const C_GUM = 'GUM';
    const C_GTM = 'GTM';
    const C_GGY = 'GGY';
    const C_GIN = 'GIN';
    const C_GNB = 'GNB';
    const C_GUY = 'GUY';
    const C_HTI = 'HTI';
    const C_HMD = 'HMD';
    const C_VAT = 'VAT';
    const C_HND = 'HND';
    const C_HKG = 'HKG';
    const C_HUN = 'HUN';
    const C_ISL = 'ISL';
    const C_IND = 'IND';
    const C_IDN = 'IDN';
    const C_IRN = 'IRN';
    const C_IRQ = 'IRQ';
    const C_IRL = 'IRL';
    const C_IMN = 'IMN';
    const C_ISR = 'ISR';
    const C_ITA = 'ITA';
    const C_JAM = 'JAM';
    const C_JPN = 'JPN';
    const C_JEY = 'JEY';
    const C_JOR = 'JOR';
    const C_KAZ = 'KAZ';
    const C_KEN = 'KEN';
    const C_KIR = 'KIR';
    const C_PRK = 'PRK';
    const C_KOR = 'KOR';
    const C_KWT = 'KWT';
    const C_KGZ = 'KGZ';
    const C_LAO = 'LAO';
    const C_LVA = 'LVA';
    const C_LBN = 'LBN';
    const C_LSO = 'LSO';
    const C_LBR = 'LBR';
    const C_LBY = 'LBY';
    const C_LIE = 'LIE';
    const C_LTU = 'LTU';
    const C_LUX = 'LUX';
    const C_MAC = 'MAC';
    const C_MKD = 'MKD';
    const C_MDG = 'MDG';
    const C_MWI = 'MWI';
    const C_MYS = 'MYS';
    const C_MDV = 'MDV';
    const C_MLI = 'MLI';
    const C_MLT = 'MLT';
    const C_MHL = 'MHL';
    const C_MTQ = 'MTQ';
    const C_MRT = 'MRT';
    const C_MUS = 'MUS';
    const C_MYT = 'MYT';
    const C_MEX = 'MEX';
    const C_FSM = 'FSM';
    const C_MDA = 'MDA';
    const C_MCO = 'MCO';
    const C_MNG = 'MNG';
    const C_MNE = 'MNE';
    const C_MSR = 'MSR';
    const C_MAR = 'MAR';
    const C_MOZ = 'MOZ';
    const C_MMR = 'MMR';
    const C_NAM = 'NAM';
    const C_NRU = 'NRU';
    const C_NPL = 'NPL';
    const C_NLD = 'NLD';
    const C_NCL = 'NCL';
    const C_NZL = 'NZL';
    const C_NIC = 'NIC';
    const C_NER = 'NER';
    const C_NGA = 'NGA';
    const C_NIU = 'NIU';
    const C_NFK = 'NFK';
    const C_MNP = 'MNP';
    const C_NOR = 'NOR';
    const C_OMN = 'OMN';
    const C_PAK = 'PAK';
    const C_PLW = 'PLW';
    const C_PSE = 'PSE';
    const C_PAN = 'PAN';
    const C_PNG = 'PNG';
    const C_PRY = 'PRY';
    const C_PER = 'PER';
    const C_PHL = 'PHL';
    const C_PCN = 'PCN';
    const C_POL = 'POL';
    const C_PRT = 'PRT';
    const C_PRI = 'PRI';
    const C_QAT = 'QAT';
    const C_REU = 'REU';
    const C_ROU = 'ROU';
    const C_RUS = 'RUS';
    const C_RWA = 'RWA';
    const C_BLM = 'BLM';
    const C_SHN = 'SHN';
    const C_KNA = 'KNA';
    const C_LCA = 'LCA';
    const C_MAF = 'MAF';
    const C_SPM = 'SPM';
    const C_VCT = 'VCT';
    const C_WSM = 'WSM';
    const C_SMR = 'SMR';
    const C_STP = 'STP';
    const C_SAU = 'SAU';
    const C_SEN = 'SEN';
    const C_SRB = 'SRB';
    const C_SYC = 'SYC';
    const C_SLE = 'SLE';
    const C_SGP = 'SGP';
    const C_SXM = 'SXM';
    const C_SVK = 'SVK';
    const C_SVN = 'SVN';
    const C_SLB = 'SLB';
    const C_SOM = 'SOM';
    const C_ZAF = 'ZAF';
    const C_SGS = 'SGS';
    const C_SSD = 'SSD';
    const C_ESP = 'ESP';
    const C_LKA = 'LKA';
    const C_SDN = 'SDN';
    const C_SUR = 'SUR';
    const C_SJM = 'SJM';
    const C_SWZ = 'SWZ';
    const C_SWE = 'SWE';
    const C_CHE = 'CHE';
    const C_SYR = 'SYR';
    const C_TWN = 'TWN';
    const C_TJK = 'TJK';
    const C_TZA = 'TZA';
    const C_THA = 'THA';
    const C_TLS = 'TLS';
    const C_TGO = 'TGO';
    const C_TKL = 'TKL';
    const C_TON = 'TON';
    const C_TTO = 'TTO';
    const C_TUN = 'TUN';
    const C_TUR = 'TUR';
    const C_TKM = 'TKM';
    const C_TCA = 'TCA';
    const C_TUV = 'TUV';
    const C_UGA = 'UGA';
    const C_UKR = 'UKR';
    const C_ARE = 'ARE';
    const C_GBR = 'GBR';
    const C_USA = 'USA';
    const C_UMI = 'UMI';
    const C_URY = 'URY';
    const C_UZB = 'UZB';
    const C_VUT = 'VUT';
    const C_VEN = 'VEN';
    const C_VNM = 'VNM';
    const C_VGB = 'VGB';
    const C_VIR = 'VIR';
    const C_WLF = 'WLF';
    const C_ESH = 'ESH';
    const C_YEM = 'YEM';
    const C_ZMB = 'ZMB';
    const C_ZWE = 'ZWE';
}