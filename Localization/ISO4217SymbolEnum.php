<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace phpOMS\Localization;

use phpOMS\Datatypes\Enum;

/**
 * Country symbols ISO list.
 *
 * @category   Framework
 * @package    phpOMS\DataStorage\Database
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class ISO4217SymbolEnum extends Enum
{
    const C_ALL = 'Lek';
    const C_AFN = '؋';
    const C_ARS = '$';
    const C_AWG = 'ƒ';
    const C_AUD = '$';
    const C_AZN = 'ман';
    const C_BSD = '$';
    const C_BBD = '$';
    const C_BYR = 'p.';
    const C_BZD = 'BZ$';
    const C_BMD = '$';
    const C_BOB = '$b';
    const C_BAM = 'KM';
    const C_BWP = 'P';
    const C_BGN = 'лв';
    const C_BRL = 'R$';
    const C_BND = '$';
    const C_KHR = '៛';
    const C_CAD = '$';
    const C_KYD = '$';
    const C_CLP = '$';
    const C_CNY = '¥';
    const C_COP = '$';
    const C_CRC = '₡';
    const C_HRK = 'kn';
    const C_CUP = '₱';
    const C_CZK = 'Kč';
    const C_DKK = 'kr';
    const C_DOP = 'RD$';
    const C_XCD = '$';
    const C_EGP = '£';
    const C_SVC = '$';
    const C_EUR = '€';
    const C_FKP = '£';
    const C_FJD = '$';
    const C_GHS = '¢';
    const C_GIP = '£';
    const C_GTQ = 'Q';
    const C_GGP = '£';
    const C_GYD = '$';
    const C_HNL = 'L';
    const C_HKD = '$';
    const C_HUF = 'Ft';
    const C_ISK = 'kr';
    const C_INR = 'INR';
    const C_IDR = 'Rp';
    const C_IRR = '﷼';
    const C_IMP = '£';
    const C_ILS = '₪';
    const C_JMD = 'J$';
    const C_JPY = '¥';
    const C_JEP = '£';
    const C_KZT = 'лв';
    const C_KPW = '₩';
    const C_KRW = '₩';
    const C_KGS = 'лв';
    const C_LAK = '₭';
    const C_LBP = '£';
    const C_LRD = '$';
    const C_MKD = 'ден';
    const C_MYR = 'RM';
    const C_MUR = '₨';
    const C_MXN = '$';
    const C_MNT = '₮';
    const C_MZN = 'MT';
    const C_NAD = '$';
    const C_NPR = '₨';
    const C_ANG = 'ƒ';
    const C_NZD = '$';
    const C_NIO = 'C$';
    const C_NGN = '₦';
    const C_KPW = '₩';
    const C_NOK = 'kr';
    const C_OMR = '﷼';
    const C_PKR = '₨';
    const C_PAB = 'B/.';
    const C_PYG = 'Gs';
    const C_PEN = 'S/.';
    const C_PHP = '₱';
    const C_PLN = 'zł';
    const C_QAR = '﷼';
    const C_RON = 'lei';
    const C_RUB = 'руб';
    const C_SHP = '£';
    const C_SAR = '﷼';
    const C_RSD = 'Дин.';
    const C_SCR = '₨';
    const C_SGD = '$';
    const C_SBD = '$';
    const C_SOS = 'S';
    const C_ZAR = 'R';
    const C_KRW = '₩';
    const C_LKR = '₨';
    const C_SEK = 'kr';
    const C_CHF = 'CHF';
    const C_SRD = '$';
    const C_SYP = '£';
    const C_TWD = 'NT$';
    const C_THB = '฿';
    const C_TTD = 'TT$';
    const C_TRY = 'TRY';
    const C_TVD = '$';
    const C_UAH = '₴';
    const C_GBP = '£';
    const C_USD = '$';
    const C_UYU = '$U';
    const C_UZS = 'лв';
    const C_VEF = 'Bs';
    const C_VND = '₫';
    const C_YER = '﷼';
    const C_ZWD = 'Z$';
}