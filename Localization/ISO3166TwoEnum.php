<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace phpOMS\Localization;

use phpOMS\Datatypes\EnumArray;

/**
 * Country codes ISO list.
 *
 * @category   Framework
 * @package    phpOMS\DataStorage\Database
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class ISO3166TwoEnum extends EnumArray
{
    const C_AFG = 'AF';
    const C_ALA = 'AX';
    const C_ALB = 'AL';
    const C_DZA = 'DZ';
    const C_ASM = 'AS';
    const C_AND = 'AD';
    const C_AGO = 'AO';
    const C_AIA = 'AI';
    const C_ATA = 'AQ';
    const C_ATG = 'AG';
    const C_ARG = 'AR';
    const C_ARM = 'AM';
    const C_ABW = 'AW';
    const C_AUS = 'AU';
    const C_AUT = 'AT';
    const C_AZE = 'AZ';
    const C_BHS = 'BS';
    const C_BHR = 'BH';
    const C_BGD = 'BD';
    const C_BRB = 'BB';
    const C_BLR = 'BY';
    const C_BEL = 'BE';
    const C_BLZ = 'BZ';
    const C_BEN = 'BJ';
    const C_BMU = 'BM';
    const C_BTN = 'BT';
    const C_BOL = 'BO';
    const C_BES = 'BQ';
    const C_BIH = 'BA';
    const C_BWA = 'BW';
    const C_BVT = 'BV';
    const C_BRA = 'BR';
    const C_IOT = 'IO';
    const C_BRN = 'BN';
    const C_BGR = 'BG';
    const C_BFA = 'BF';
    const C_BDI = 'BI';
    const C_CPV = 'CV';
    const C_KHM = 'KH';
    const C_CMR = 'CM';
    const C_CAN = 'CA';
    const C_CYM = 'KY';
    const C_CAF = 'CF';
    const C_TCD = 'TD';
    const C_CHL = 'CL';
    const C_CHN = 'CN';
    const C_CXR = 'CX';
    const C_CCK = 'CC';
    const C_COL = 'CO';
    const C_COM = 'KM';
    const C_COG = 'CG';
    const C_COD = 'CD';
    const C_COK = 'CK';
    const C_CRI = 'CR';
    const C_CIV = 'CI';
    const C_HRV = 'HR';
    const C_CUB = 'CU';
    const C_CUW = 'CW';
    const C_CYP = 'CY';
    const C_CZE = 'CZ';
    const C_DNK = 'DK';
    const C_DJI = 'DJ';
    const C_DMA = 'DM';
    const C_DOM = 'DO';
    const C_ECU = 'EC';
    const C_EGY = 'EG';
    const C_SLV = 'SV';
    const C_GNQ = 'GQ';
    const C_ERI = 'ER';
    const C_EST = 'EE';
    const C_ETH = 'ET';
    const C_FLK = 'FK';
    const C_FRO = 'FO';
    const C_FJI = 'FJ';
    const C_FIN = 'FI';
    const C_FRA = 'FR';
    const C_GUF = 'GF';
    const C_PYF = 'PF';
    const C_ATF = 'TF';
    const C_GAB = 'GA';
    const C_GMB = 'GM';
    const C_GEO = 'GE';
    const C_DEU = 'DE';
    const C_GHA = 'GH';
    const C_GIB = 'GI';
    const C_GRC = 'GR';
    const C_GRL = 'GL';
    const C_GRD = 'GD';
    const C_GLP = 'GP';
    const C_GUM = 'GU';
    const C_GTM = 'GT';
    const C_GGY = 'GG';
    const C_GIN = 'GN';
    const C_GNB = 'GW';
    const C_GUY = 'GY';
    const C_HTI = 'HT';
    const C_HMD = 'HM';
    const C_VAT = 'VA';
    const C_HND = 'HN';
    const C_HKG = 'HK';
    const C_HUN = 'HU';
    const C_ISL = 'IS';
    const C_IND = 'IN';
    const C_IDN = 'ID';
    const C_IRN = 'IR';
    const C_IRQ = 'IQ';
    const C_IRL = 'IE';
    const C_IMN = 'IM';
    const C_ISR = 'IL';
    const C_ITA = 'IT';
    const C_JAM = 'JM';
    const C_JPN = 'JP';
    const C_JEY = 'JE';
    const C_JOR = 'JO';
    const C_KAZ = 'KZ';
    const C_KEN = 'KE';
    const C_KIR = 'KI';
    const C_PRK = 'KP';
    const C_KOR = 'KR';
    const C_KWT = 'KW';
    const C_KGZ = 'KG';
    const C_LAO = 'LA';
    const C_LVA = 'LV';
    const C_LBN = 'LB';
    const C_LSO = 'LS';
    const C_LBR = 'LR';
    const C_LBY = 'LY';
    const C_LIE = 'LI';
    const C_LTU = 'LT';
    const C_LUX = 'LU';
    const C_MAC = 'MO';
    const C_MKD = 'MK';
    const C_MDG = 'MG';
    const C_MWI = 'MW';
    const C_MYS = 'MY';
    const C_MDV = 'MV';
    const C_MLI = 'ML';
    const C_MLT = 'MT';
    const C_MHL = 'MH';
    const C_MTQ = 'MQ';
    const C_MRT = 'MR';
    const C_MUS = 'MU';
    const C_MYT = 'YT';
    const C_MEX = 'MX';
    const C_FSM = 'FM';
    const C_MDA = 'MD';
    const C_MCO = 'MC';
    const C_MNG = 'MN';
    const C_MNE = 'ME';
    const C_MSR = 'MS';
    const C_MAR = 'MA';
    const C_MOZ = 'MZ';
    const C_MMR = 'MM';
    const C_NAM = 'NA';
    const C_NRU = 'NR';
    const C_NPL = 'NP';
    const C_NLD = 'NL';
    const C_NCL = 'NC';
    const C_NZL = 'NZ';
    const C_NIC = 'NI';
    const C_NER = 'NE';
    const C_NGA = 'NG';
    const C_NIU = 'NU';
    const C_NFK = 'NF';
    const C_MNP = 'MP';
    const C_NOR = 'NO';
    const C_OMN = 'OM';
    const C_PAK = 'PK';
    const C_PLW = 'PW';
    const C_PSE = 'PS';
    const C_PAN = 'PA';
    const C_PNG = 'PG';
    const C_PRY = 'PY';
    const C_PER = 'PE';
    const C_PHL = 'PH';
    const C_PCN = 'PN';
    const C_POL = 'PL';
    const C_PRT = 'PT';
    const C_PRI = 'PR';
    const C_QAT = 'QA';
    const C_REU = 'RE';
    const C_ROU = 'RO';
    const C_RUS = 'RU';
    const C_RWA = 'RW';
    const C_BLM = 'BL';
    const C_SHN = 'SH';
    const C_KNA = 'KN';
    const C_LCA = 'LC';
    const C_MAF = 'MF';
    const C_SPM = 'PM';
    const C_VCT = 'VC';
    const C_WSM = 'WS';
    const C_SMR = 'SM';
    const C_STP = 'ST';
    const C_SAU = 'SA';
    const C_SEN = 'SN';
    const C_SRB = 'RS';
    const C_SYC = 'SC';
    const C_SLE = 'SL';
    const C_SGP = 'SG';
    const C_SXM = 'SX';
    const C_SVK = 'SK';
    const C_SVN = 'SI';
    const C_SLB = 'SB';
    const C_SOM = 'SO';
    const C_ZAF = 'ZA';
    const C_SGS = 'GS';
    const C_SSD = 'SS';
    const C_ESP = 'ES';
    const C_LKA = 'LK';
    const C_SDN = 'SD';
    const C_SUR = 'SR';
    const C_SJM = 'SJ';
    const C_SWZ = 'SZ';
    const C_SWE = 'SE';
    const C_CHE = 'CH';
    const C_SYR = 'SY';
    const C_TWN = 'TW';
    const C_TJK = 'TJ';
    const C_TZA = 'TZ';
    const C_THA = 'TH';
    const C_TLS = 'TL';
    const C_TGO = 'TG';
    const C_TKL = 'TK';
    const C_TON = 'TO';
    const C_TTO = 'TT';
    const C_TUN = 'TN';
    const C_TUR = 'TR';
    const C_TKM = 'TM';
    const C_TCA = 'TC';
    const C_TUV = 'TV';
    const C_UGA = 'UG';
    const C_UKR = 'UA';
    const C_ARE = 'AE';
    const C_GBR = 'GB';
    const C_USA = 'US';
    const C_UMI = 'UM';
    const C_URY = 'UY';
    const C_UZB = 'UZ';
    const C_VUT = 'VU';
    const C_VEN = 'VE';
    const C_VNM = 'VN';
    const C_VGB = 'VG';
    const C_VIR = 'VI';
    const C_WLF = 'WF';
    const C_ESH = 'EH';
    const C_YEM = 'YE';
    const C_ZMB = 'ZM';
    const C_ZWE = 'ZW';
}