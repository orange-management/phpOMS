<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace phpOMS\Localization;

use phpOMS\Datatypes\Enum;

/**
 * Language codes ISO list.
 *
 * @category   Framework
 * @package    phpOMS\DataStorage\Database
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class ISO639x1Enum extends Enum
{
    const _AA = 'aa';
    const _AB = 'ab';
    const _AF = 'af';
    const _AK = 'ak';
    const _SQ = 'sq';
    const _AM = 'am';
    const _AR = 'ar';
    const _AN = 'an';
    const _HY = 'hy';
    const _AS = 'as';
    const _AV = 'av';
    const _AE = 'ae';
    const _AY = 'ay';
    const _AZ = 'az';
    const _BM = 'bm';
    const _BA = 'ba';
    const _EU = 'eu';
    const _BE = 'be';
    const _BN = 'bn';
    const _BH = 'bh';
    const _BI = 'bi';
    const _BS = 'bs';
    const _BR = 'br';
    const _BG = 'bg';
    const _MY = 'my';
    const _CA = 'ca';
    const _CH = 'ch';
    const _CE = 'ce';
    const _NY = 'ny';
    const _CV = 'cv';
    const _KW = 'kw';
    const _CO = 'co';
    const _CR = 'cr';
    const _HR = 'hr';
    const _CS = 'cs';
    const _DA = 'da';
    const _DV = 'dv';
    const _NL = 'nl';
    const _DZ = 'dz';
    const _EN = 'en';
    const _EO = 'eo';
    const _ET = 'et';
    const _EE = 'ee';
    const _FO = 'fo';
    const _FJ = 'fj';
    const _FI = 'fi';
    const _FR = 'fr';
    const _FF = 'ff';
    const _GL = 'gl';
    const _KA = 'ka';
    const _DE = 'de';
    const _EL = 'el';
    const _GN = 'gn';
    const _GU = 'gu';
    const _HT = 'ht';
    const _HA = 'ha';
    const _HE = 'he';
    const _HZ = 'hz';
    const _HI = 'hi';
    const _HO = 'ho';
    const _HU = 'hu';
    const _IA = 'ia';
    const _ID = 'id';
    const _IE = 'ie';
    const _GA = 'ga';
    const _IG = 'ig';
    const _IK = 'ik';
    const _IO = 'io';
    const _IS = 'is';
    const _IT = 'it';
    const _IU = 'iu';
    const _JV = 'jv';
    const _KL = 'kl';
    const _KN = 'kn';
    const _KR = 'kr';
    const _KK = 'kk';
    const _KM = 'km';
    const _KI = 'ki';
    const _RW = 'rw';
    const _KY = 'ky';
    const _KV = 'kv';
    const _KG = 'kg';
    const _KJ = 'kj';
    const _LA = 'la';
    const _LB = 'lb';
    const _LG = 'lg';
    const _LI = 'li';
    const _LN = 'ln';
    const _LO = 'lo';
    const _LT = 'lt';
    const _LV = 'lv';
    const _GV = 'gv';
    const _MK = 'mk';
    const _MG = 'mg';
    const _ML = 'ml';
    const _MT = 'mt';
    const _MI = 'mi';
    const _MR = 'mr';
    const _MH = 'mh';
    const _MN = 'mn';
    const _NA = 'na';
    const _NV = 'nv';
    const _ND = 'nd';
    const _NE = 'ne';
    const _NG = 'ng';
    const _NB = 'nb';
    const _NN = 'nn';
    const _NO = 'no';
    const _II = 'ii';
    const _NR = 'nr';
    const _OC = 'oc';
    const _OJ = 'oj';
    const _CU = 'cu';
    const _OM = 'om';
    const _OR = 'or';
    const _OS = 'os';
    const _PI = 'pi';
    const _FA = 'fa';
    const _PL = 'pl';
    const _PS = 'ps';
    const _PT = 'pt';
    const _QU = 'qu';
    const _RM = 'rm';
    const _RN = 'rn';
    const _RO = 'ro';
    const _RU = 'ru';
    const _SA = 'sa';
    const _SC = 'sc';
    const _SE = 'se';
    const _SM = 'sm';
    const _SG = 'sg';
    const _SR = 'sr';
    const _GD = 'gd';
    const _SN = 'sn';
    const _SI = 'si';
    const _SK = 'sk';
    const _SL = 'sl';
    const _SO = 'so';
    const _ST = 'st';
    const _ES = 'es';
    const _SU = 'su';
    const _SW = 'sw';
    const _SS = 'ss';
    const _SV = 'sv';
    const _TA = 'ta';
    const _TE = 'te';
    const _TH = 'th';
    const _TI = 'ti';
    const _BO = 'bo';
    const _TK = 'tk';
    const _TL = 'tl';
    const _TN = 'tn';
    const _TO = 'to';
    const _TR = 'tr';
    const _TS = 'ts';
    const _TW = 'tw';
    const _TY = 'ty';
    const _UK = 'uk';
    const _UR = 'ur';
    const _VE = 've';
    const _VI = 'vi';
    const _VO = 'vo';
    const _WA = 'wa';
    const _CY = 'cy';
    const _WO = 'wo';
    const _FY = 'fy';
    const _XH = 'xh';
    const _YI = 'yi';
    const _YO = 'yo';
    const _ZA = 'za';
    const _ZU = 'zu';
}
