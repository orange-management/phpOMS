<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$CORELANG[0] = [
    'Add'        => 'Add',
    'Cancel'     => 'Cancel',
    'Close'      => 'Close',
    'Create'     => 'Create',
    'Delete'     => 'Delete',
    'Edit'       => 'Edit',
    'Email'      => 'Email',
    'Empty'      => 'Empty',
    'Filter'     => 'Filter',
    'Find'       => 'Find',
    'ID'         => 'ID',
    'More'       => 'More',
    'Navigation' => 'Navigation',
    'Password'   => 'Password',
    'Reset'      => 'Reset',
    'Save'       => 'Save',
    'Search'     => 'Search',
    'Select'     => 'Select',
    'Send'       => 'Send',
    'Submit'     => 'Submit',
    'e:empty'    => "This field mustn't be empty.",
    'Monday'     => 'Monday',
    'Tuesday'    => 'Tuesday',
    'Wednesday'  => 'Wednesday',
    'Thursday'   => 'Thursday',
    'Friday'     => 'Friday',
    'Saturday'   => 'Saturday',
    'Sunday'     => 'Sunday',
];
