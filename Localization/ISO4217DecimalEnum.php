
<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace phpOMS\Localization;

use phpOMS\Datatypes\Enum;

/**
 * Country codes ISO list.
 *
 * @category   Framework
 * @package    phpOMS\DataStorage\Database
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class ISO4217DecimalEnum extends Enum
{
    const C_AED = 2;
    const C_AFN = 2;
    const C_ALL = 2;
    const C_AMD = 2;
    const C_ANG = 2;
    const C_AOA = 2;
    const C_ARS = 2;
    const C_AUD = 2;
    const C_AWG = 2;
    const C_AZN = 2;
    const C_BAM = 2;
    const C_BBD = 2;
    const C_BDT = 2;
    const C_BGN = 2;
    const C_BHD = 3;
    const C_BIF = 0;
    const C_BMD = 2;
    const C_BND = 2;
    const C_BOB = 2;
    const C_BOV = 2;
    const C_BRL = 2;
    const C_BSD = 2;
    const C_BTN = 2;
    const C_BWP = 2;
    const C_BYR = 0;
    const C_BZD = 2;
    const C_CAD = 2;
    const C_CDF = 2;
    const C_CHE = 2;
    const C_CHF = 2;
    const C_CHW = 2;
    const C_CLF = 4;
    const C_CLP = 0;
    const C_CNY = 2;
    const C_COP = 2;
    const C_COU = 2;
    const C_CRC = 2;
    const C_CUC = 2;
    const C_CUP = 2;
    const C_CVE = 0;
    const C_CZK = 2;
    const C_DJF = 0;
    const C_DKK = 2;
    const C_DOP = 2;
    const C_DZD = 2;
    const C_EGP = 2;
    const C_ERN = 2;
    const C_ETB = 2;
    const C_EUR = 2;
    const C_FJD = 2;
    const C_FKP = 2;
    const C_GBP = 2;
    const C_GEL = 2;
    const C_GHS = 2;
    const C_GIP = 2;
    const C_GMD = 2;
    const C_GNF = 0;
    const C_GTQ = 2;
    const C_GYD = 2;
    const C_HKD = 2;
    const C_HNL = 2;
    const C_HRK = 2;
    const C_HTG = 2;
    const C_HUF = 2;
    const C_IDR = 2;
    const C_ILS = 2;
    const C_INR = 2;
    const C_IQD = 3;
    const C_IRR = 2;
    const C_ISK = 0;
    const C_JMD = 2;
    const C_JOD = 3;
    const C_JPY = 0;
    const C_KES = 2;
    const C_KGS = 2;
    const C_KHR = 2;
    const C_KMF = 0;
    const C_KPW = 2;
    const C_KRW = 0;
    const C_KWD = 3;
    const C_KYD = 2;
    const C_KZT = 2;
    const C_LAK = 2;
    const C_LBP = 2;
    const C_LKR = 2;
    const C_LRD = 2;
    const C_LSL = 2;
    const C_LYD = 3;
    const C_MAD = 2;
    const C_MDL = 2;
    const C_MGA = 1;
    const C_MKD = 2;
    const C_MMK = 2;
    const C_MNT = 2;
    const C_MOP = 2;
    const C_MRO = 1;
    const C_MUR = 2;
    const C_MVR = 2;
    const C_MWK = 2;
    const C_MXN = 2;
    const C_MXV = 2;
    const C_MYR = 2;
    const C_MZN = 2;
    const C_NAD = 2;
    const C_NGN = 2;
    const C_NIO = 2;
    const C_NOK = 2;
    const C_NPR = 2;
    const C_NZD = 2;
    const C_OMR = 3;
    const C_PAB = 2;
    const C_PEN = 2;
    const C_PGK = 2;
    const C_PHP = 2;
    const C_PKR = 2;
    const C_PLN = 2;
    const C_PYG = 0;
    const C_QAR = 2;
    const C_RON = 2;
    const C_RSD = 2;
    const C_RUB = 2;
    const C_RWF = 0;
    const C_SAR = 2;
    const C_SBD = 2;
    const C_SCR = 2;
    const C_SDG = 2;
    const C_SEK = 2;
    const C_SGD = 2;
    const C_SHP = 2;
    const C_SLL = 2;
    const C_SOS = 2;
    const C_SRD = 2;
    const C_SSP = 2;
    const C_STD = 2;
    const C_SYP = 2;
    const C_SZL = 2;
    const C_THB = 2;
    const C_TJS = 2;
    const C_TMT = 2;
    const C_TND = 3;
    const C_TOP = 2;
    const C_TRY = 2;
    const C_TTD = 2;
    const C_TWD = 2;
    const C_TZS = 2;
    const C_UAH = 2;
    const C_UGX = 0;
    const C_USD = 2;
    const C_USN = 2;
    const C_USS = 2;
    const C_UYI = 0;
    const C_UYU = 2;
    const C_UZS = 2;
    const C_VEF = 2;
    const C_VND = 0;
    const C_VUV = 0;
    const C_WST = 2;
    const C_XAF = 0;
    const C_XAG = -1;
    const C_XAU = -1;
    const C_XBA = -1;
    const C_XBB = -1;
    const C_XBC = -1;
    const C_XBD = -1;
    const C_XCD = 2;
    const C_XDR = -1;
    const C_XFU = -1;
    const C_XOF = 0;
    const C_XPD = -1;
    const C_XPF = 0;
    const C_XPT = -1;
    const C_XSU = -1;
    const C_XTS = -1;
    const C_XUA = -1;
    const C_XXX = -1;
    const C_YER = 2;
    const C_ZAR = 2;
    const C_ZMW = 2;
}