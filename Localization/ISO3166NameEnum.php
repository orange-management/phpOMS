<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace phpOMS\Localization;

use phpOMS\Datatypes\Enum;

/**
 * Country codes ISO list.
 *
 * @category   Framework
 * @package    phpOMS\DataStorage\Database
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class ISO3166NameEnum extends Enum
{
    const C_AFG = 'Afghanistan';
    const C_ALA = 'Åland Islands';
    const C_ALB = 'Albania';
    const C_DZA = 'Algeria';
    const C_ASM = 'American Samoa';
    const C_AND = 'Andorra';
    const C_AGO = 'Angola';
    const C_AIA = 'Anguilla';
    const C_ATA = 'Antarctica';
    const C_ATG = 'Antigua and Barbuda';
    const C_ARG = 'Argentina';
    const C_ARM = 'Armenia';
    const C_ABW = 'Aruba';
    const C_AUS = 'Australia';
    const C_AUT = 'Austria';
    const C_AZE = 'Azerbaijan';
    const C_BHS = 'Bahamas';
    const C_BHR = 'Bahrain';
    const C_BGD = 'Bangladesh';
    const C_BRB = 'Barbados';
    const C_BLR = 'Belarus';
    const C_BEL = 'Belgium';
    const C_BLZ = 'Belize';
    const C_BEN = 'Benin';
    const C_BMU = 'Bermuda';
    const C_BTN = 'Bhutan';
    const C_BOL = 'Bolivia (Plurinational State of)';
    const C_BES = 'Bonaire, Sint Eustatius and Saba';
    const C_BIH = 'Bosnia and Herzegovina';
    const C_BWA = 'Botswana';
    const C_BVT = 'Bouvet Island';
    const C_BRA = 'Brazil';
    const C_IOT = 'British Indian Ocean Territory';
    const C_BRN = 'Brunei Darussalam';
    const C_BGR = 'Bulgaria';
    const C_BFA = 'Burkina Faso';
    const C_BDI = 'Burundi';
    const C_CPV = 'Cabo Verde';
    const C_KHM = 'Cambodia';
    const C_CMR = 'Cameroon';
    const C_CAN = 'Canada';
    const C_CYM = 'Cayman Islands';
    const C_CAF = 'Central African Republic';
    const C_TCD = 'Chad';
    const C_CHL = 'Chile';
    const C_CHN = 'China';
    const C_CXR = 'Christmas Island';
    const C_CCK = 'Cocos (Keeling) Islands';
    const C_COL = 'Colombia';
    const C_COM = 'Comoros';
    const C_COG = 'Congo';
    const C_COD = 'Congo (Democratic Republic of the)';
    const C_COK = 'Cook Islands';
    const C_CRI = 'Costa Rica';
    const C_CIV = 'Côte d\'Ivoire';
    const C_HRV = 'Croatia';
    const C_CUB = 'Cuba';
    const C_CUW = 'Curaçao';
    const C_CYP = 'Cyprus';
    const C_CZE = 'Czech Republic';
    const C_DNK = 'Denmark';
    const C_DJI = 'Djibouti';
    const C_DMA = 'Dominica';
    const C_DOM = 'Dominican Republic';
    const C_ECU = 'Ecuador';
    const C_EGY = 'Egypt';
    const C_SLV = 'El Salvador';
    const C_GNQ = 'Equatorial Guinea';
    const C_ERI = 'Eritrea';
    const C_EST = 'Estonia';
    const C_ETH = 'Ethiopia';
    const C_FLK = 'Falkland Islands (Malvinas)';
    const C_FRO = 'Faroe Islands';
    const C_FJI = 'Fiji';
    const C_FIN = 'Finland';
    const C_FRA = 'France';
    const C_GUF = 'French Guiana';
    const C_PYF = 'French Polynesia';
    const C_ATF = 'French Southern Territories';
    const C_GAB = 'Gabon';
    const C_GMB = 'Gambia';
    const C_GEO = 'Georgia';
    const C_DEU = 'Germany';
    const C_GHA = 'Ghana';
    const C_GIB = 'Gibraltar';
    const C_GRC = 'Greece';
    const C_GRL = 'Greenland';
    const C_GRD = 'Grenada';
    const C_GLP = 'Guadeloupe';
    const C_GUM = 'Guam';
    const C_GTM = 'Guatemala';
    const C_GGY = 'Guernsey';
    const C_GIN = 'Guinea';
    const C_GNB = 'Guinea-Bissau';
    const C_GUY = 'Guyana';
    const C_HTI = 'Haiti';
    const C_HMD = 'Heard Island and McDonald Islands';
    const C_VAT = 'Holy See';
    const C_HND = 'Honduras';
    const C_HKG = 'Hong Kong';
    const C_HUN = 'Hungary';
    const C_ISL = 'Iceland';
    const C_IND = 'India';
    const C_IDN = 'Indonesia';
    const C_IRN = 'Iran (Islamic Republic of)';
    const C_IRQ = 'Iraq';
    const C_IRL = 'Ireland';
    const C_IMN = 'Isle of Man';
    const C_ISR = 'Israel';
    const C_ITA = 'Italy';
    const C_JAM = 'Jamaica';
    const C_JPN = 'Japan';
    const C_JEY = 'Jersey';
    const C_JOR = 'Jordan';
    const C_KAZ = 'Kazakhstan';
    const C_KEN = 'Kenya';
    const C_KIR = 'Kiribati';
    const C_PRK = 'Korea (Democratic People\'s Republic of)';
    const C_KOR = 'Korea (Republic of)';
    const C_KWT = 'Kuwait';
    const C_KGZ = 'Kyrgyzstan';
    const C_LAO = 'Lao People\'s Democratic Republic';
    const C_LVA = 'Latvia';
    const C_LBN = 'Lebanon';
    const C_LSO = 'Lesotho';
    const C_LBR = 'Liberia';
    const C_LBY = 'Libya';
    const C_LIE = 'Liechtenstein';
    const C_LTU = 'Lithuania';
    const C_LUX = 'Luxembourg';
    const C_MAC = 'Macao';
    const C_MKD = 'Macedonia (the former Yugoslav Republic of)';
    const C_MDG = 'Madagascar';
    const C_MWI = 'Malawi';
    const C_MYS = 'Malaysia';
    const C_MDV = 'Maldives';
    const C_MLI = 'Mali';
    const C_MLT = 'Malta';
    const C_MHL = 'Marshall Islands';
    const C_MTQ = 'Martinique';
    const C_MRT = 'Mauritania';
    const C_MUS = 'Mauritius';
    const C_MYT = 'Mayotte';
    const C_MEX = 'Mexico';
    const C_FSM = 'Micronesia (Federated States of)';
    const C_MDA = 'Moldova (Republic of)';
    const C_MCO = 'Monaco';
    const C_MNG = 'Mongolia';
    const C_MNE = 'Montenegro';
    const C_MSR = 'Montserrat';
    const C_MAR = 'Morocco';
    const C_MOZ = 'Mozambique';
    const C_MMR = 'Myanmar';
    const C_NAM = 'Namibia';
    const C_NRU = 'Nauru';
    const C_NPL = 'Nepal';
    const C_NLD = 'Netherlands';
    const C_NCL = 'New Caledonia';
    const C_NZL = 'New Zealand';
    const C_NIC = 'Nicaragua';
    const C_NER = 'Niger';
    const C_NGA = 'Nigeria';
    const C_NIU = 'Niue';
    const C_NFK = 'Norfolk Island';
    const C_MNP = 'Northern Mariana Islands';
    const C_NOR = 'Norway';
    const C_OMN = 'Oman';
    const C_PAK = 'Pakistan';
    const C_PLW = 'Palau';
    const C_PSE = 'Palestine, State of';
    const C_PAN = 'Panama';
    const C_PNG = 'Papua New Guinea';
    const C_PRY = 'Paraguay';
    const C_PER = 'Peru';
    const C_PHL = 'Philippines';
    const C_PCN = 'Pitcairn';
    const C_POL = 'Poland';
    const C_PRT = 'Portugal';
    const C_PRI = 'Puerto Rico';
    const C_QAT = 'Qatar';
    const C_REU = 'Réunion';
    const C_ROU = 'Romania';
    const C_RUS = 'Russian Federation';
    const C_RWA = 'Rwanda';
    const C_BLM = 'Saint Barthélemy';
    const C_SHN = 'Saint Helena, Ascension and Tristan da Cunha';
    const C_KNA = 'Saint Kitts and Nevis';
    const C_LCA = 'Saint Lucia';
    const C_MAF = 'Saint Martin (French part)';
    const C_SPM = 'Saint Pierre and Miquelon';
    const C_VCT = 'Saint Vincent and the Grenadines';
    const C_WSM = 'Samoa';
    const C_SMR = 'San Marino';
    const C_STP = 'Sao Tome and Principe';
    const C_SAU = 'Saudi Arabia';
    const C_SEN = 'Senegal';
    const C_SRB = 'Serbia';
    const C_SYC = 'Seychelles';
    const C_SLE = 'Sierra Leone';
    const C_SGP = 'Singapore';
    const C_SXM = 'Sint Maarten (Dutch part)';
    const C_SVK = 'Slovakia';
    const C_SVN = 'Slovenia';
    const C_SLB = 'Solomon Islands';
    const C_SOM = 'Somalia';
    const C_ZAF = 'South Africa';
    const C_SGS = 'South Georgia and the South Sandwich Islands';
    const C_SSD = 'South Sudan';
    const C_ESP = 'Spain';
    const C_LKA = 'Sri Lanka';
    const C_SDN = 'Sudan';
    const C_SUR = 'Suriname';
    const C_SJM = 'Svalbard and Jan Mayen';
    const C_SWZ = 'Swaziland';
    const C_SWE = 'Sweden';
    const C_CHE = 'Switzerland';
    const C_SYR = 'Syrian Arab Republic';
    const C_TWN = 'Taiwan, Province of China[a]';
    const C_TJK = 'Tajikistan';
    const C_TZA = 'Tanzania, United Republic of';
    const C_THA = 'Thailand';
    const C_TLS = 'Timor-Leste';
    const C_TGO = 'Togo';
    const C_TKL = 'Tokelau';
    const C_TON = 'Tonga';
    const C_TTO = 'Trinidad and Tobago';
    const C_TUN = 'Tunisia';
    const C_TUR = 'Turkey';
    const C_TKM = 'Turkmenistan';
    const C_TCA = 'Turks and Caicos Islands';
    const C_TUV = 'Tuvalu';
    const C_UGA = 'Uganda';
    const C_UKR = 'Ukraine';
    const C_ARE = 'United Arab Emirates';
    const C_GBR = 'United Kingdom of Great Britain and Northern Ireland';
    const C_USA = 'United States of America';
    const C_UMI = 'United States Minor Outlying Islands';
    const C_URY = 'Uruguay';
    const C_UZB = 'Uzbekistan';
    const C_VUT = 'Vanuatu';
    const C_VEN = 'Venezuela (Bolivarian Republic of)';
    const C_VNM = 'Viet Nam';
    const C_VGB = 'Virgin Islands (British)';
    const C_VIR = 'Virgin Islands (U.S.)';
    const C_WLF = 'Wallis and Futuna';
    const C_ESH = 'Western Sahara';
    const C_YEM = 'Yemen';
    const C_ZMB = 'Zambia';
    const C_ZWE = 'Zimbabwe';
}