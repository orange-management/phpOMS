
<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace phpOMS\Localization;

use phpOMS\Datatypes\Enum;

/**
 * Country codes ISO list.
 *
 * @category   Framework
 * @package    phpOMS\DataStorage\Database
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class ISO4217DecimalEnum extends Enum
{
    const C_AED = '784';
    const C_AFN = '971';
    const C_ALL = '008';
    const C_AMD = '051';
    const C_ANG = '532';
    const C_AOA = '973';
    const C_ARS = '032';
    const C_AUD = '036';
    const C_AWG = '533';
    const C_AZN = '944';
    const C_BAM = '977';
    const C_BBD = '052';
    const C_BDT = '050';
    const C_BGN = '975';
    const C_BHD = '048';
    const C_BIF = '108';
    const C_BMD = '060';
    const C_BND = '096';
    const C_BOB = '068';
    const C_BOV = '984';
    const C_BRL = '986';
    const C_BSD = '044';
    const C_BTN = '064';
    const C_BWP = '072';
    const C_BYR = '974';
    const C_BZD = '084';
    const C_CAD = '124';
    const C_CDF = '976';
    const C_CHE = '947';
    const C_CHF = '756';
    const C_CHW = '948';
    const C_CLF = '990';
    const C_CLP = '152';
    const C_CNY = '156';
    const C_COP = '170';
    const C_COU = '970';
    const C_CRC = '188';
    const C_CUC = '931';
    const C_CUP = '192';
    const C_CVE = '132';
    const C_CZK = '203';
    const C_DJF = '262';
    const C_DKK = '208';
    const C_DOP = '214';
    const C_DZD = '012';
    const C_EGP = '818';
    const C_ERN = '232';
    const C_ETB = '230';
    const C_EUR = '978';
    const C_FJD = '242';
    const C_FKP = '238';
    const C_GBP = '826';
    const C_GEL = '981';
    const C_GHS = '936';
    const C_GIP = '292';
    const C_GMD = '270';
    const C_GNF = '324';
    const C_GTQ = '320';
    const C_GYD = '328';
    const C_HKD = '344';
    const C_HNL = '340';
    const C_HRK = '191';
    const C_HTG = '332';
    const C_HUF = '348';
    const C_IDR = '360';
    const C_ILS = '376';
    const C_INR = '356';
    const C_IQD = '368';
    const C_IRR = '364';
    const C_ISK = '352';
    const C_JMD = '388';
    const C_JOD = '400';
    const C_JPY = '392';
    const C_KES = '404';
    const C_KGS = '417';
    const C_KHR = '116';
    const C_KMF = '174';
    const C_KPW = '408';
    const C_KRW = '410';
    const C_KWD = '414';
    const C_KYD = '136';
    const C_KZT = '398';
    const C_LAK = '418';
    const C_LBP = '422';
    const C_LKR = '144';
    const C_LRD = '430';
    const C_LSL = '426';
    const C_LYD = '434';
    const C_MAD = '504';
    const C_MDL = '498';
    const C_MGA = '969';
    const C_MKD = '807';
    const C_MMK = '104';
    const C_MNT = '496';
    const C_MOP = '446';
    const C_MRO = '478';
    const C_MUR = '480';
    const C_MVR = '462';
    const C_MWK = '454';
    const C_MXN = '484';
    const C_MXV = '979';
    const C_MYR = '458';
    const C_MZN = '943';
    const C_NAD = '516';
    const C_NGN = '566';
    const C_NIO = '558';
    const C_NOK = '578';
    const C_NPR = '524';
    const C_NZD = '554';
    const C_OMR = '512';
    const C_PAB = '590';
    const C_PEN = '604';
    const C_PGK = '598';
    const C_PHP = '608';
    const C_PKR = '586';
    const C_PLN = '985';
    const C_PYG = '600';
    const C_QAR = '634';
    const C_RON = '946';
    const C_RSD = '941';
    const C_RUB = '643';
    const C_RWF = '646';
    const C_SAR = '682';
    const C_SBD = '090';
    const C_SCR = '690';
    const C_SDG = '938';
    const C_SEK = '752';
    const C_SGD = '702';
    const C_SHP = '654';
    const C_SLL = '694';
    const C_SOS = '706';
    const C_SRD = '968';
    const C_SSP = '728';
    const C_STD = '678';
    const C_SYP = '760';
    const C_SZL = '748';
    const C_THB = '764';
    const C_TJS = '972';
    const C_TMT = '934';
    const C_TND = '788';
    const C_TOP = '776';
    const C_TRY = '949';
    const C_TTD = '780';
    const C_TWD = '901';
    const C_TZS = '834';
    const C_UAH = '980';
    const C_UGX = '800';
    const C_USD = '840';
    const C_USN = '997';
    const C_USS = '998';
    const C_UYI = '940';
    const C_UYU = '858';
    const C_UZS = '860';
    const C_VEF = '937';
    const C_VND = '704';
    const C_VUV = '548';
    const C_WST = '882';
    const C_XAF = '950';
    const C_XAG = '961';
    const C_XAU = '959';
    const C_XBA = '955';
    const C_XBB = '956';
    const C_XBC = '957';
    const C_XBD = '958';
    const C_XCD = '951';
    const C_XDR = '960';
    const C_XFU = 'Nil';
    const C_XOF = '952';
    const C_XPD = '964';
    const C_XPF = '953';
    const C_XPT = '962';
    const C_XSU = '994';
    const C_XTS = '963';
    const C_XUA = '965';
    const C_XXX = '999';
    const C_YER = '886';
    const C_ZAR = '710';
    const C_ZMW = '967';
}